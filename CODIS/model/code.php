<?php

class Code{

    private $_code;
    private $_val;

    public function __construct($c = "", $v = ""){
        $this->setCode($c);
        $this->setVal($v);
    }

    public function getCode(){
        return $this->_code;
    }

    public function getVal(){
        return $this->_val;
    }

    public function setCode($c){
        $this->_code = $c;
    }

    public function setVal($v){
        $this->_val = $v;
    }

}
