<?php

require_once(__DIR__.'/../controller/IndexController.php');
$cnt = new IndexController();
$list = $cnt->generateList();

?><html>
    <head>
        <title>Codes</title>
    </head>
    <body>
        <div id="wrapper">
            <h1>Codes</h1>
            <table>
                <tr><th>Code</th><th></th></tr>
                <?php foreach($list as $l) { ?>
                <tr>
                    <td><?=$l->getCode()?></td>
                    <td><a href="/details.php?code=<?=$l->getCode()?>">Ver detalles</a></td>
                </tr>
                <?php } ?>
            </table>
        </div>
    </body>


</html>