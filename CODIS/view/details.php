<?php
require_once(__DIR__.'/../controller/IndexController.php');
$cnt = new IndexController();

$code = $_GET['code'];
$element = $cnt->generateDetails($code);
?><html>
    <head>
        <title>Details</title>
    </head>
    <body>
        <div id="wrapper">
            <h1>Details of <?=$element->getCode()?></h1>
            <ul>
                <li>Code: <?=$element->getCode()?></li>
                <li>Reference: <?=$element->getVal()?></li>
            </ul>
        </div>
    </body>
</html>
