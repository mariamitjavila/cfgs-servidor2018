<?php

require_once(__DIR__.'/../MODEL/Frase.php');


class IndexController{

  private $autorFeli = ['Buda', 'Dalai Lama', 'Albert Einstein','Plató'];
  private $autorEsp = ['Pep Guardiola', 'Bruce Lee', 'Muhammad Ali'];
  private $autorMus = ['Txarango'];

  private $fraseEsp = ['Pep Guardiola' => ['Tenemos que ser audaces,salir al campo y hacer
  las cosas, no sentarnos y esperar a que suceda.', 'El talento depende de la inspiración, pero el esfuerzo
  depende de cada uno.'],
  'Bruce Lee' =>['Todos tenemos tiempo para aprovechar o para perder y es nuestra decisión
  qué es lo que hacemos con él. Pero ten en cuenta que una vez pasadom jamás se recupera.', 'No temas fallar.
  No es fallar, sino apuntar muy bajo el error. Con grandes aspiraciones, es glorioso incluso fallar.'],
  'Muhammad Ali' =>['Aquél que no es lo suficientemente valiente como para tomar riesgos,
  no logrará nada en la vida.', 'Detesté cada minuto de entrenamiento, pero me dije: No renuncies, sufre
  ahora y vive el resto de tu vida como un campeón.']
  ];

  private $fraseFeli = ['Buda' =>['El dolor es inevitable pero el sufrimiento es opcional', 'Todo lo que somos
  es el resultado de lo que hemos pensado, está fundado en nuestros pensamientos y está hecho de nuestros
  pensamientos.'],
  'Dalai Lama' =>['Si dominamos nuestra mente, vendrá la felicidad.'],
  'Albert Einstein' =>['Solo hay dos formas de vivir tu vida. Una es pensar que nada es un milagro.
  La otra es pensar que todo es un milagro.'],
  'Plató' =>['Buscando el bien de nuestros semejantes, encontraremos el nuestro.']
  ];

  private $fraseMus = ['Txarango' =>['Que la fi del món, no tingui final de vida.',
  "Tant de bo que la sang fos germana i no l'arma dels impotents.",
  'Levantaremos un mundo mejor.',
  'Camina amb mi un petit trosset del teu camí.']
  ];

  public function frasesFelicitat ($n = 5){
    $ret = array();
    for ($i=0; $i < $n; $i++) {
      $af = $this->autorFeli[$i];
      for ($j=0; $j < count($this->fraseFeli[$af]); $j++) {
        $ff = $this->fraseFeli[$af][$j];
        $reta = new FraseFeli($af, $ff);
        array_push($ret, $reta);
      }
    }
    return $ret;
  }

  public function frasesEsport ($n = 6){
    $ret = array();
    for ($i=0; $i < $n; $i++) {
      $ae = $this->autorEsp[$i];
      for ($j=0; $j < count($this->fraseEsp[$ae]); $j++) {
        $fe = $this->fraseEsp[$ae][$j];
        $reta = new FraseEsp($ae, $fe);
        array_push($ret, $reta);
      }
    }
    return $ret;
  }

  public function frasesMusica($n = 4){
    $ret = array();
    for ($i=0; $i < $n; $i++) {
      $am = $this->autorMus[$i];
      for ($j=0; $j < count($this->fraseMus[$am]); $j++) {
        $fm = $this->fraseMus[$am][$j];
        $reta = new FraseMus($am, $fm);
        array_push($ret, $reta);
      }
    }
    return $ret;
  }

}
