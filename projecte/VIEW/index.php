<?php

require_once(__DIR__.'/../CONTROLLER/IndexController.php');

$cnt = new IndexController();
$fraselistF = $cnt -> frasesFelicitat();
$fraselistE = $cnt -> frasesEsport();
$fraselistM = $cnt -> frasesMusica();

?>

<html>
<title>PROJECTE 1</title>
<meta charset="UTF-8">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Karma">
<link rel="stylesheet" href="index.css" type="text/css">

<head>
  <script>
    function w3_open() {
        document.getElementById("mySidebar").style.display = "block";
    }

    function w3_close() {
        document.getElementById("mySidebar").style.display = "none";
    }
    </script>
</head>

<body>
<!-- MENU -->
<nav class="w3-sidebar w3-bar-block w3-card w3-top w3-xlarge w3-animate-left" style="display:none;z-index:2;width:40%;min-width:300px" id="mySidebar">
  <a href="javascript:void(0)" onclick="w3_close()"
  class="w3-bar-item w3-button">TANCA MENU</a>
  <a href="#frasesA" onclick="w3_close()" class="w3-bar-item w3-button">FRASES DE FELICITAT</a>
  <a href="#frasesB" onclick="w3_close()" class="w3-bar-item w3-button">FRASES DE ESPORTS</a>
  <a href="#frasesC" onclick="w3_close()" class="w3-bar-item w3-button">FRASES DE MÚSICA</a>
</nav>

<!-- MENU -->
<div class="w3-top">
  <div class="w3-white w3-xlarge" style="max-width:1200px;margin:auto">
    <div class="w3-button w3-padding-16 w3-left" onclick="w3_open()">☰</div>
    <div class="w3-center w3-padding-16"><h1>EL LLISTAT DE FRASES</h1></div>
  </div>
</div>

<!-- !PAGE CONTENT! -->
<div class="w3-main w3-content w3-padding" style="max-width:1200px;margin-top:100px">

  <!-- FRASES FELICITAT -->
    <div class="w3-container w3-padding-32 w3-center">
      <hr id="frasesA">
      <h2>LA FELICITAT EN FRASES</h2><br>
      <div class="w3-row-padding w3-padding-16 w3-center">
        <div id="wrapper">
          <img src="FOTOS/felicitat2.jpg" alt="foto">
          <br>
          <br>
          <br>
          <div id="table">
            <table>
              <tr><th>FRASES </th> <th> AUTOR</th></tr>
               <?php foreach($fraselistF as $frase){ ?>
                <tr>
                  <td><?=$frase->getFraseFeli() ?></td>
                  <td><?=$frase->getAutorFeli() ?></td>
                </tr>
              <?php } ?>
            </table>
          </div>
        </div>
    </div>
    </div>
    </hr>

    <!-- FRASES ESPORTS -->
      <div class="w3-container w3-padding-32 w3-center">
        <hr id="frasesB">
        <h2>L'ESPORT ENS PARLA</h2><br>
        <div class="w3-row-padding w3-padding-16 w3-center">
          <div id="imatges">
            <div class="cont-imatge">
                <img src="FOTOS/esports1.jpg" alt="foto">
            </div>
            <div class="cont-imatge">
              <img src="FOTOS/esports3.jpg" alt="foto">
            </div>
            <div class="cont-imatge">
              <img src="FOTOS/esports2.jpg" alt="foto">
            </div>
          </div>
          <br>
          <div id="table">
            <table>
              <tr><th>FRASES </th> <th> AUTOR</th></tr>
               <?php foreach($fraselistE as $frase){ ?>
                <tr>
                  <td><?=$frase->getFraseEsp() ?></td>
                  <td><?=$frase->getAutorEsp() ?></td>
                </tr>
              <?php } ?>
            </table>
          </div>
      </div>
      </div>
      </hr>

    <!-- FRASES MUSICA -->
      <div class="w3-container w3-padding-32 w3-center">
          <hr id="frasesC">
        <h2>LA MÚSICA EN PARAULES</h2><br>
        <div class="w3-row-padding w3-padding-16 w3-center">
          <div class="">
            <img src="FOTOS/music.jpg" alt="foto">
          </div>
          <br>
          <div id="table">
            <table>
              <tr><th>FRASES </th> <th> AUTOR</th></tr>
               <?php foreach($fraselistM as $frase){ ?>
                <tr>
                  <td><?=$frase->getFraseMus() ?></td>
                  <td><?=$frase->getAutorMus() ?></td>
                </tr>
              <?php } ?>
            </table>
          </div>
      </div>
      </div>
      </hr>

    <!--FOOTER -->
      <footer class="w3-row-padding w3-padding-32">
        <div class="w3-third">
          <p>MARIA MITJAVILA</p>
          <p>2n DAW-BIO</p>
          <p>DESENVOLUPAMENT WEB ENTORN SERVIDOR</p>
          Powered by <a href="https://www.w3schools.com/w3css/default.asp" target="_blank">w3.css</a>
        </div>
      </footer>

</div>
</body>
</html>
