<?php

class FraseFeli{

  private $autorFeli;
  private $fraseFeli;

  public function __construct($af, $ff){
    $this -> autorFeli = $af;
    $this -> fraseFeli = $ff;
  }

  public function getAutorFeli(){
    return $this->autorFeli;
  }

  public function getFraseFeli(){
    return $this->fraseFeli;
  }

  public function setAutorFeli($autorFeli){
      $this->autorFeli = $autorFeli;
      return $this;
  }

  public function setFraseFeli($fraseFeli){
      $this->fraseFeli = $fraseFeli;
      return $this;
  }
}

class FraseEsp{

  private $autorEsp;
  private $fraseEsp;

  public function __construct($ae, $fe){
    $this -> autorEsp = $ae;
    $this -> fraseEsp = $fe;
  }

  public function getAutorEsp(){
    return $this->autorEsp;
  }

  public function getFraseEsp(){
    return $this->fraseEsp;
  }

  public function setAutorEsp($autorEsp){
      $this->autorEsp = $autorEsp;
      return $this;
  }

  public function setFraseEsp($fraseEsp){
      $this->fraseEsp = $fraseEsp;
      return $this;
  }
}

class FraseMus{

  private $autorMus;
  private $fraseMus;

  public function __construct($am, $fm){
    $this -> autorMus = $am;
    $this -> fraseMus = $fm;
  }

  public function getAutorMus(){
    return $this->autorMus;
  }

  public function getFraseMus(){
    return $this->fraseMus;
  }

  public function setAutorMus($autorMus){
      $this->autorMus = $autorMus;
      return $this;
  }

  public function setFraseMus($fraseMus){
      $this->fraseMus = $fraseMus;
      return $this;
  }
}
