<?php
require_once(__DIR__.'/../model/db/GameCDb.php');

class GameCController{

    public function createCharacter($name, $specie, $attack=null, $defense=null){
        $gc = new GameC($name, $specie);
        $gc->setDefenseP($defense);
        $gc->setAttackP($attack);

        $db = new GameCDb();
        return $db->createGameC($gc);
    }

    public function listCharacters(){
        $db = new GameCDb();
        return $db->listGameC();
    }

    public function getCharacter($id){
        $db = new GameCDb();
        return $db->getGameC($id);
    }

    public function modifyCharacter(){

    }

    public function removeCharacter(){

    }

}
