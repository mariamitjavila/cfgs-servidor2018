<?php

require_once(__DIR__.'/../model/db/CurrencyDb.php');

class IndexController{

    public function getCurrencies(){
        $db = new CurrencyDb();
        return $db->getCurrencies();
    }

}
