<?php

require_once(__DIR__.'/../../controller/IndexController.php');
require_once(__DIR__.'/../../inc/Constants.php');

$cn = $_POST['cname'];
$cs = $_POST['csymb'];
$cev = $_POST['cev'];
$ct = $_POST['ctype'];

$cnt = new IndexController();

$currency = $cnt->createCurrency($cn, $cs, $cev, $ct);

?><html>
	<head>
		<title>Currency created</title>
	</head>
	<body>
		<div id="wrapper">
			<h1><?=$currency->getName()?></h1>
			<ul>
				<li><?=$currency->getSymbol()?></li>
				<li><?=$currency->getEurval()?></li>
				<li><?=Constants::CURRTYPE[$currency->getType()]?></li>
			</ul>
		</div>
	</body>
</html>
