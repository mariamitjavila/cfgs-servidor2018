<?php

require_once(__DIR__.'/../controller/IndexController.php');
$cnt = new IndexController();

$list = $cnt->getCurrencies();

?><html>
	<head>
		<title>Currencies</title>
	</head>
	<body>
		<div id="wrapper">
			<h1>Currencies List</h1>
			<ul>
			<?php foreach($list as $c){ ?>
			<li><strong><?=$c->getName()?></strong></li>
			<li><?=$c->getSymbol()?></li>
			<li><?=$c->getEurval()?></li>
			<li><?=$c->getType()?></li>
			<li></li>
			<?php } ?>
			</ul>
		</div>
	</body>
</html>
