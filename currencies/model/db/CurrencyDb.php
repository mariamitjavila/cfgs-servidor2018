<?php

require_once(__DIR__.'/../Currency.php');

class CurrencyDb{

  private $conn;

  public function getCurrencies(){
    $this->openConnection();

    $sql = 'SELECT * FROM currency';
    $stm =$this->conn->prepare($sql);

    $stm->execute();
    $result = $stm-> get_result();

    $ret = array();
    while ($r = $result->fetch_assoc()) {
      $curr = new Currency($r['symbol'], $r['name'], $r['eurval'], $r['type']);
      array_push($ret, $curr);
    }
    return $ret;
  }

  private function openConnection(){
    if($this->conn == null){
      $this->conn = mysql_connect("127.0.0.1",
                                  "money",
                                  "Money!123",
                                  "money");
    }
  }
}
