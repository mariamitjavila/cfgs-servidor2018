<?php

    require_once(__DIR__.'/../controller/IndexController.php');

    $cnt = new IndexController();
    $carlist = $cnt->randomListAction();

    ?><html>
<head>
<title>MVC Car Sample</title>
</head>
<body>
<div id="wrapper">
<h1>MVC Car Sample</h1>
<div id="inputform">
<form method="post" action="accio.php">
<dl>
<dl><label for="marcain">Marca</label></dl>
<dd><select id="marcain" name="marca">
<option value="BMW">BMW</option>
<option value="Mercedes">Mercedes</option>
<option value="VW">VW</option>
<option value="Renault">Renault</option>
</select></dd>
<dl><label for="modelin">Model</label></dl>
<dd><select id="modelin" name="model">
<option value="m3">m3</option>
<option value="m30">m30</option>
<option value="z1">z1</option>
<option value="Serie 1">Serie 1</option>
<option value="AMG">AMG</option>
<option value="SLK">SLK</option>
<option value="SLS">SLS</option>
<option value="AMG GT">SMG GT</option>
<option value="Golf">Golf</option>
<option value="Polo">Polo</option>
<option value="Sirocco">Sirocco</option>
<option value="Ciclo">Ciclo</option>
<option value="Megane">Megane</option>
<option value="Espace">Espace</option>
<option value="Scenic">Scecnic</option>
<option value="Twingo">Twingo</option>
</select></dd>
<dl><label for="combin">Combustible</label></dl>
<dd><select id="combin" name="combustible">
<option value="unleaded">unleaded</option>
<option value="Diesel">Diesel</option>
</select></dd>
<dl><label for="colorin">Color</label></dl>
<dd><select id="colorin" name="color">
<option value="red">Red</option>
<option value="blue">Blue</option>
<option value="white">White</option>
<option value="black">Black</option>
</select></dd>
<dl>
<div id="enviar">
<input type="submit" value="ENVIAR DADES" id="boto_enviar" name="boto_enviar"/>
</div>
</form>
</div>
<table>
<tr><th>Marca</th><th>Model</th><th>Combustible</th><th>Color</th></tr>
<?php foreach($carlist as $car){ ?>
<tr>
<td><?=$car->getBrand()?></td>
<td><?=$car->getModel()?></td>
<td><?=$car->getGas()?></td>
<td><?=$car->getColor()?></td>
</tr>
<?php } ?>
</table>
</div>
</body>
</html>
