<?php

class Car{

    private $_brand;
    private $_model;
    private $_gas;
    private $_color;

    public function __construct($b, $m, $g, $c){
        $this->_brand = $b;
        $this->_model = $m;
        $this->_gas = $g;
        $this->_color = $c;
    }

    public function getBrand(){
        return $this->_brand;
    }

    public function getModel(){
        return $this->_model;
    }

    public function getGas(){
        return $this->_gas;
    }

    public function getColor(){
        return $this->_color;
    }

}
